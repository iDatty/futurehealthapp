import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './vuex/index.js'
import vuetify from './plugins/vuetify';
const jwt = require('jsonwebtoken');

Vue.use(require('vue-cookies'))

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  created () {
    const token = Vue.$cookies.get('fh_token')
    if (token) { // check to see if there is indeed a user
      const userdata = jwt.verify(token, process.env.VUE_APP_JWT_SECRET)
      if(userdata) {
      this.$store.commit('SET_USER_DATA', userdata) // restore user data with Vuex
      }
    }
  },
  render: h => h(App)
}).$mount('#app')
