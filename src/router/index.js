import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard.vue'
import axios from 'axios'
import store from '../vuex'
const jwt = require('jsonwebtoken');

Vue.use(VueRouter)
Vue.use(require('vue-cookies'))

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/suppliers',
    name: 'Suppliers',
    component: () => import('../views/Suppliers.vue')
  },
  {
    path: '/suppliers/:id',
    name: 'Supplier',
    component: () => import('../views/Supplier.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
    meta: {
      noAuth: true 
  }
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue'),
    meta: {
      noAuth: true 
  }
  },
  {
    path: '/my-profile',
    name: 'MyProfile',
    component: () => import('../views/MyProfile.vue'),
  },
  {
    path: '/my-company',
    name: 'MyCompany',
    component: () => import('../views/MyCompany.vue'),
  },
  {
    path: '/my-content/',
    name: 'MyContent',
    component: () => import('../views/MyContent.vue')
  },
  {
    path: '/new-content/',
    name: 'NewContent',
    component: () => import('../views/NewContent.vue')
  },
  {
    path: '/content/:id',
    name: 'Content',
    component: () => import('../views/Content.vue')
  },
  {
    path: '/my-content/edit/:id',
    name: 'EditContent',
    component: () => import('../views/EditContent.vue')
  },
  {
    path: '/my-inbox',
    name: 'MyInbox',
    component: () => import('../views/MyInbox.vue'),
  },
  {
    path: '/my-inbox/:id',
    name: 'Conversation',
    component: () => import('../views/Conversation.vue'),
  },
  {
    path: '/new-conversation/:id',
    name: 'NewConversation',
    component: () => import('../views/NewConversation.vue'),
    beforeEnter: (to, from, next) => {
      if( from.name == 'Supplier' ) {
        next()
      } else {
        next({
          path: '/'
        })
      }
    }
  },
  { path: "*",
    name: 'PageNotFound',
    component: () => import('../views/PageNotFound.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  const token = Vue.$cookies.get('fh_token')
    if(to.matched.some(record => !record.meta.noAuth)) {
      if (!token) {
          next({
              path: '/login',
              params: { nextUrl: to.fullPath }
          })
      } else {
          const decoded = jwt.verify(token, process.env.VUE_APP_JWT_SECRET)
            if(decoded){
              axios.get(`/api/login/auth/`, {
                params: {
                  ID: decoded._id
                }
            }).then((response) => {
                store.commit('SET_USER_DATA', response.data)
                })
                .catch((err)=> {
                    console.log(err);
                })
              next()
            } else {
              sessionStorage.clear();
              next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            })
            }

    }
  } else {
      next()
  }
  });

export default router
