import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)
Vue.use(require('vue-cookies'))

Vue.$cookies.config('30d')

export default new Vuex.Store({
    plugins: [createPersistedState({
        storage: window.sessionStorage,
    })],
  state: {
    user: null,
    loginmsg: null
  },
  mutations: {
    SET_USER_DATA (state, userData) {
      state.user = userData
    },
    SET_LOGIN_MSG(state, message) {
      state.loginmsg = message
    }
  },
  actions: {
    login ({ commit }, credentials) {
      return axios
        .post(`/api/login`, credentials)
        .then(({ data }) => {
          Vue.$cookies.set('fh_token', data.token)
          commit('SET_USER_DATA', data.user)
        }) .catch(err => {
          commit('SET_LOGIN_MSG', err.response.data.message)
        }) 
    },
    logout () {
      Vue.$cookies.remove('fh_token')
      sessionStorage.clear();
      location.reload()
    },
    updateUserState ({ commit }, data) {
          commit('SET_USER_DATA', data)
    }
  },
  getters: {
    loggedIn (state) {
      return !!state.user
    },
    userType (state) {
      return state.user.type
    }
  }
})