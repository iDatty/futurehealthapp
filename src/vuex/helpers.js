import { mapGetters } from 'vuex'
import { mapState } from 'vuex'
    
export const authComputed = {
  ...mapGetters(['loggedIn']),
  ...mapGetters(['userType'])
}

export const userState = {
  ...mapState({
    user: state => state.user
  })
}