import axios from 'axios';
const url = `/api/content/`;
import format from 'date-fns/format'


class ContentService {

    // Get My Content
    static getMyContent(id) {
            return new Promise ((resolve,reject) => {
                axios.get(url, {
                    params: {
                      ID: id
                    }
                }).then((res) => {
                    const data = res.data;
                    resolve(
                        data.map(content => ({
                            ...content,
                            date: format(new Date(content.date), 'dd/MM/yyyy')
                        }))
                    );
                })
                .catch((err)=> {
                    reject(err);
                })
            });

    }

    // Get My Content To Edit
    static getContent(id) {
        return new Promise ((resolve,reject) => {
            axios.get(url+'content', {
                params: {
                  ID: id
                }
            }).then((res) => {
                const data = res.data;
                resolve(
                    data.map(content => ({
                        ...content
                    }))
                );
            })
            .catch((err)=> {
                reject(err);
            })
        });
    }

    // Get My Content To Edit
    static getMyContentEdit(id) {
        return new Promise ((resolve,reject) => {
            axios.get(url+'edit', {
                params: {
                  ID: id
                }
            }).then((res) => {
                const data = res.data;
                resolve(
                    data.map(content => ({
                        ...content
                    }))
                );
            })
            .catch((err)=> {
                reject(err);
            })
        });
    }

    // Create Content
    static createContent(content) {
        return axios.post(url+'new', content, {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
        })
    }

    // Update Content
    static updateContent(content) {
        return axios.put(url+'edit', content, {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
        })
    }

    static deleteContent(obj) {
        return axios.delete(url+'delete', {
            params: {
                ID : obj.id,
                companyid : obj.companyid
            }
        })
    }

}

export default ContentService;