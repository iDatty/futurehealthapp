import axios from 'axios';
const url = `/api/user/`;



class UserService {

    // Get Users
    static getUser(id) {
            return new Promise ((resolve,reject) => {
                axios.get(url, {
                    params: {
                      ID: id
                    }
                }).then((res) => {
                    const data = res.data;
                    resolve(
                        data.map(user => ({
                            ...user,
                            password: null
                        }))
                    );
                })
                .catch((err)=> {
                    reject(err);
                })
            });

    }

    // Update User
    static updateUser(user) {
        return axios.put(url, user, {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
        })
    }

}

export default UserService;