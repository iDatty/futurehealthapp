const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();
const bcrypt = require('bcryptjs');
const axios = require('axios')

// Insert User

router.post('/', async (req, res) => {
    const data = req.body;
    const users = await loadUsersCollection();
    return users.findOne({email:data.email})
           .then(user => {
               if (!user) {
                bcrypt.hash(data.password, 10).then( async function(hash) {
                    const userObj = {
                        "username" : data.email,
                        "password" : hash,
                        "salutation" : data.salutation,
                        "firstname" : data.firstname,
                        "lastname" : data.lastname,
                        "email" : data.email,
                        "phonecountry" : data.phonecountry,
                        "phone" : data.phone,
                        "company" : data.company,
                        "companyrole" : data.companyrole,
                        "website" : data.website,
                        "address" : {
                            "line1" : data.address.line1,
                            "line2" : data.address.line2,
                            "city" : data.address.city,
                            "postcode" : data.address.postcode,
                            "region" : data.address.region,
                            "country" : data.address.country
                        },
                        "avatar" : "",
                        "type" : "buyer"
                    }
                let insertUser = await users.insertOne(userObj);
                res.status(200).json({"message":"Account created successfully","userid":insertUser.insertedId,"type":"success"});

                // send to zoho
                delete userObj.password
                axios.post('https://webhook.site/af20dcf0-faa8-42d9-bfd8-258bd72b6873',userObj)

            })
               } else {
                   res.json({"message":"Email already exists","type":"error"})
               }
            })

})

const loadUsersCollection = async () => {
    const client = await mongodb.MongoClient.connect(`mongodb://${process.env.VUE_APP_MONGODB_USER}:${process.env.VUE_APP_MONGODB_PW}@${process.env.VUE_APP_MONGODB_IP}/futurehealth`, {
        useNewUrlParser: true
    });

    return client.db('futurehealth').collection('users');
}

module.exports = router;