import axios from 'axios';
const url = `/api/conversation/`;



class NewConversationService {

    // Check Conversations
    static checkConversations(users) {
            return new Promise ((resolve,reject) => {
                axios.get(url, {
                    params: users
                }).then((res) => {
                    const data = res.data;
                    resolve(
                        data
                    );
                })
                .catch((err)=> {
                    reject(err);
                })
            });

    }

    // Post Message
    static postMessage(param) {
        axios.post(url, {
            param
          })
          .then(function (response) {
            console.log(response.data.ops[0].conversationid);
            window.location.href = `/my-inbox/${response.data.ops[0].conversationid}`;
          })
          .catch(function (error) {
            console.log(error);
          });
    }
}

export default NewConversationService;