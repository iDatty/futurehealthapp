const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();
var aws = require('aws-sdk')
var multer = require('multer')
var multerS3 = require('multer-s3')
var s3 = new aws.S3()

aws.config.update({
    secretAccessKey: process.env.VUE_APP_S3_SECRET,
    accessKeyId: process.env.VUE_APP_S3_ID,
    region: 'us-east-1'
});

var upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'futurehealthlive',
    acl: 'public-read',
    metadata: function (req, file, cb) {
      cb(null, {type: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, `users/${req.body._id}/avatar`)
    }
  })
})
 
// Get User

router.get('/', async (req, res) => {
    const users = await loadUsersCollection();
    res.send(await users.find({_id:mongodb.ObjectId(req.query.ID)}).toArray());
})

// Update User

router.put('/', upload.single('avatar'), async (req, res) => {
    const data = req.body;
    const address = JSON.parse(data.address)
    if (req.file) {
    var avatarurl = req.file.location
    } else {
    var avatarurl = data.avatar
    }
    const users = await loadUsersCollection();
    await users.updateOne({_id:mongodb.ObjectID(data._id)},
    { $set: {
        "salutation" : data.salutation,
        "firstname" : data.firstname,
        "lastname" : data.lastname,
        "email" : data.email,
        "phonecountry" : data.phonecountry,
        "phone" : data.phone,
        "company" : data.company,
        "companyrole" : data.companyrole,
        "website" : data.website,
        "address" : {
            "line1" : address.line1,
            "line2" : address.line2,
            "city" : address.city,
            "postcode" : address.postcode,
            "region" : address.region,
            "country" : address.country
        },
        "avatar" : avatarurl
    } }
    );
    res.status(200).send();

})

const loadUsersCollection = async () => {
    const client = await mongodb.MongoClient.connect(`mongodb://${process.env.VUE_APP_MONGODB_USER}:${process.env.VUE_APP_MONGODB_PW}@${process.env.VUE_APP_MONGODB_IP}/futurehealth`, {
        useNewUrlParser: true
    });

    return client.db('futurehealth').collection('users');
}

module.exports = router;