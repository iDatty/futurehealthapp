import axios from 'axios';
const url = `/api/company/`;



class CompanyService {

    // Get Company
    static getCompany(id) {
            return new Promise ((resolve,reject) => {
                axios.get(url, {
                    params: {
                      ID: id
                    }
                }).then((res) => {
                    const data = res.data;
                    resolve(
                        data.map(company => ({
                            ...company
                        }))
                    );
                })
                .catch((err)=> {
                    reject(err);
                })
            });

    }

    // Get All Companies
    static getCompanies() {
        return new Promise ((resolve,reject) => {
            axios.get(`${url}suppliers`).then((res) => {
                const data = res.data;
                resolve(
                    data.map(company => ({
                        ...company
                    }))
                );
            })
            .catch((err)=> {
                reject(err);
            })
        });

}

    // Get Company from param
    static getCompanyParam(param) {
        return new Promise ((resolve,reject) => {
            axios.get(`${url}suppliers/${param}`).then((res) => {
                const data = res.data;
                resolve(
                    data.map(company => ({
                        ...company
                    }))
                );
            })
            .catch((err)=> {
                reject(err);
            })
        });

}

    // Update Company
    static updateCompany(company) {
        return axios.put(url, company, {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
        })
    }

}

export default CompanyService;