const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();
var aws = require('aws-sdk')
var multer = require('multer')
var multerS3 = require('multer-s3')
var s3 = new aws.S3()

aws.config.update({
    secretAccessKey: process.env.VUE_APP_S3_SECRET,
    accessKeyId: process.env.VUE_APP_S3_ID,
    region: 'us-east-1'
});

var upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'futurehealthlive',
    acl: 'public-read',
    metadata: function (req, file, cb) {
      cb(null, {type: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, `companies/${req.body._id}/logo`)
    }
  })
})

// Get Company

router.get('/', async (req, res) => {
    const companies = await loadCompaniesCollection();
    res.send(await companies.find({userid:req.query.ID}).toArray());
})

// Get All Companies

router.get('/suppliers', async (req, res) => {
    const companies = await loadCompaniesCollection();
    res.send(await companies.find({}).toArray());
})

// Get Company from param

router.get('/suppliers/:ID', async (req, res) => {
    const companies = await loadCompaniesCollection();
    res.send(await companies.find({_id:mongodb.ObjectID(req.params.ID)}).toArray());
})

// Update Company

router.put('/', upload.single('logourl'), async (req, res) => {
    const data = req.body;
    const address = JSON.parse(data.address)
    if (req.file) {
        var logourl = req.file.location
        } else {
        var logourl = data.logourl
        }
    const companies = await loadCompaniesCollection();
    await companies.updateOne({_id:mongodb.ObjectID(data._id)},
    { $set: {
        "email" : data.email,
        "phonecountry" : data.phonecountry,
        "phone" : data.phone,
        "companyname" : data.companyname,
        "website" : data.website,
        "address" : {
            "line1" : address.line1,
            "line2" : address.line2,
            "city" : address.city,
            "postcode" : address.postcode,
            "region" : address.region,
            "country" : address.country
        },
        "operatingregion" : [],
        "description" : data.description,
        "category" : data.category,
        "subcategory" : data.subcategory,
        "videourl" : '',
        "gallery" : [],
        "logourl" : logourl
    } }
    );
    res.status(200).send();
})

const loadCompaniesCollection = async () => {
    const client = await mongodb.MongoClient.connect(`mongodb://${process.env.VUE_APP_MONGODB_USER}:${process.env.VUE_APP_MONGODB_PW}@${process.env.VUE_APP_MONGODB_IP}/futurehealth`, {
        useNewUrlParser: true
    });

    return client.db('futurehealth').collection('companies');
}

module.exports = router;