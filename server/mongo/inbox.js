const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();

// Get Conversations

router.get('/', async (req, res) => {
    const conversations = await loadConversationsCollection();
    res.send(await conversations.find({ users: req.query.ID }).toArray());
})

// Get Conversation

router.get('/conversation', async (req, res) => {
    const conversations = await loadConversationsCollection();
    res.send(await conversations.find({ _id:mongodb.ObjectId(req.query.ID) }).toArray());
})

// Get Messages

router.get('/:ID', async (req, res) => {
    const messages = await loadMessagesCollection();
    res.send(await messages.find({conversationid:req.params.ID,}).toArray());
})

// Add Message

router.post('/', async (req, res) => {
    const data = req.body.param
    const messages = await loadMessagesCollection();
    res.send(await messages.insertOne({
        conversationid:data.conversationid,
        message:data.message,
        user:data.user,
        recipient:data.recipient,
        datetime:new Date(),
        read:false}));
})

const loadConversationsCollection = async () => {
    const client = await mongodb.MongoClient.connect(`mongodb://${process.env.VUE_APP_MONGODB_USER}:${process.env.VUE_APP_MONGODB_PW}@${process.env.VUE_APP_MONGODB_IP}/futurehealth`, {
        useNewUrlParser: true
    });

    return client.db('futurehealth').collection('conversations');
}

const loadMessagesCollection = async () => {
    const client = await mongodb.MongoClient.connect(`mongodb://${process.env.VUE_APP_MONGODB_USER}:${process.env.VUE_APP_MONGODB_PW}@${process.env.VUE_APP_MONGODB_IP}/futurehealth`, {
        useNewUrlParser: true
    });

    return client.db('futurehealth').collection('messages');
}

module.exports = router;