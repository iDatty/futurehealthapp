import axios from 'axios';
const url = `/api/seller/`;



class SellerService {



    // Insert User
    static insertSeller(obj) {
        return new Promise ((resolve,reject) => {
            axios.post(url, obj).then((res) => {
                const data = res.data;
                resolve(
                    data
                );
            })
            .catch((err)=> {
                reject(err);
            })
        });

}

}

export default SellerService;