import axios from 'axios';
const url = `/api/inbox/`;
import format from 'date-fns/format'


class InboxService {

    // Get Conversations
    static getConversations(id) {
            return new Promise ((resolve,reject) => {
                axios.get(url, {
                    params: {
                      ID: id
                    }
                }).then((res) => {
                    const data = res.data;
                    resolve(
                        data.map(conversation => ({
                            ...conversation
                        }))
                    );
                })
                .catch((err)=> {
                    reject(err);
                })
            });

    }

    // Get Conversation
    static getConversation(id) {
        return new Promise ((resolve,reject) => {
            axios.get(url+'conversation', {
                params: {
                  ID: id
                }
            }).then((res) => {
                const data = res.data;
                resolve(
                    data.map(conversation => ({
                        ...conversation
                    }))
                );
            })
            .catch((err)=> {
                reject(err);
            })
        });
}

    // Get Messages from param
    static getMessagesParam(param) {
        return new Promise ((resolve,reject) => {
            axios.get(`${url}${param}`).then((res) => {
                const data = res.data;
                resolve(
                    data.map(message => ({
                        ...message,
                        datetime: format(new Date(message.datetime), 'HH:mm - dd/MM/yyyy')
                    }))
                );
            })
            .catch((err)=> {
                reject(err);
            })
        }); 
}


    // Post Message
    static postMessage(param) {
        axios.post(url, {
            param
          })
          .then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
    }
}

export default InboxService;