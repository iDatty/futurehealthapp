const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();
var aws = require('aws-sdk')
var multer = require('multer')
var multerS3 = require('multer-s3')
var s3 = new aws.S3()

aws.config.update({
    secretAccessKey: process.env.VUE_APP_S3_SECRET,
    accessKeyId: process.env.VUE_APP_S3_ID,
    region: 'us-east-1'
});

var upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'futurehealthlive',
    acl: 'public-read',
    metadata: function (req, file, cb) {
      cb(null, {type: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, `content/${req.body._id}/mainimage`)
    }
  })
})

// Get Content

router.get('/', async (req, res) => {
    const content = await loadContentCollection();
    res.send(await content.find({companyid:req.query.ID}).toArray());
})

router.post('/new',upload.single('mainimage'), async (req, res) => {
    const data = req.body;
    if (req.file) {
        var mainimageurl = req.file.location
        } else {
        var mainimageurl = data.mainimage
        }
    const content = await loadContentCollection();
    res.send(await content.insertOne({
        title:data.title,
        maintext:data.maintext,
        companyid:data.companyid,
        type:data.type,
        mainimage:mainimageurl,
        date:new Date()}));
})

// Get One Content to Edit

router.get('/edit', async (req, res) => {
    const content = await loadContentCollection();
    res.send(await content.find({_id:mongodb.ObjectId(req.query.ID)}).toArray());
})

// Get One Content to Edit

router.get('/content', async (req, res) => {
    const content = await loadContentCollection();
    res.send(await content.find({_id:mongodb.ObjectId(req.query.ID)}).toArray());
})

router.put('/edit', upload.single('mainimage'), async (req, res) => {
    const data = req.body;
    if (req.file) {
        var mainimageurl = req.file.location
        } else {
        var mainimageurl = data.mainimage
        }
    const content = await loadContentCollection();
    await content.updateOne({_id:mongodb.ObjectID(data._id)},
    { $set: {
        "title" : data.title,
        "maintext" : data.maintext,
        "mainimage" : mainimageurl,
        "date": "",
        "type": data.type,
        "companyid" : data.companyid
    } }
    );
    res.status(200).send();
})

router.delete('/delete', async (req, res) => {
    const content = await loadContentCollection();
    res.send(await content.deleteOne({_id:mongodb.ObjectId(req.query.ID),companyid:req.query.companyid}));
})

const loadContentCollection = async () => {
    const client = await mongodb.MongoClient.connect(`mongodb://${process.env.VUE_APP_MONGODB_USER}:${process.env.VUE_APP_MONGODB_PW}@${process.env.VUE_APP_MONGODB_IP}/futurehealth`, {
        useNewUrlParser: true
    });

    return client.db('futurehealth').collection('content');
}

module.exports = router;
