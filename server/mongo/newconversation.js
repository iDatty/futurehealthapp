const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();

// Check Conversation

router.get('/', async (req, res) => {
    const conversations = await loadConversationsCollection();
    return conversations.findOne({ users: { $all: [req.query.current,req.query.recipient] } })
        .then(conversation => {
            if (!conversation) {
                res.send('No Convo')
            } else {
                res.status(200).json({redirect:'/my-inbox/' + conversation._id.toString()})
            }
        })
})

// Create Conversation and Add Message

router.post('/', async (req, res) => {
    const data = req.body.param
    const conversations = await loadConversationsCollection();
    const newconversation = await conversations.insertOne({users:[data.users.current,data.users.recipient],lastmessage:''})
    const messages = await loadMessagesCollection();
    res.send(await messages.insertOne({conversationid:newconversation.insertedId.toString(),message:data.message.message,user:data.message.user,datetime:new Date(),read:false}));
})

const loadConversationsCollection = async () => {
    const client = await mongodb.MongoClient.connect(`mongodb://${process.env.VUE_APP_MONGODB_USER}:${process.env.VUE_APP_MONGODB_PW}@${process.env.VUE_APP_MONGODB_IP}/futurehealth`, {
        useNewUrlParser: true
    });

    return client.db('futurehealth').collection('conversations');
}

const loadMessagesCollection = async () => {
    const client = await mongodb.MongoClient.connect(`mongodb://${process.env.VUE_APP_MONGODB_USER}:${process.env.VUE_APP_MONGODB_PW}@${process.env.VUE_APP_MONGODB_IP}/futurehealth`, {
        useNewUrlParser: true
    });

    return client.db('futurehealth').collection('messages');
}

module.exports = router;