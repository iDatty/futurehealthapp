import axios from 'axios';
const url = `/api/buyer/`;



class BuyerService {



    // Insert User
    static insertBuyer(obj) {
        return new Promise ((resolve,reject) => {
            axios.post(url, obj).then((res) => {
                const data = res.data;
                resolve(
                    data
                );
            })
            .catch((err)=> {
                reject(err);
            })
        });

}

}

export default BuyerService;