const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
require('./passport/passport');
require('dotenv').config()
const app = express();
const cookieparser = require('cookie-parser')

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(cookieparser())

// CORS middleware
const allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', '*');
    next();
}

app.use(allowCrossDomain)

// Login & Auth
const auth = require('./passport/auth');
app.use('/api/login', auth);

// Users

const user = require('./mongo/user');
app.use('/api/user', user);

// Register Buyer

const buyer = require('./mongo/buyer');
app.use('/api/buyer', buyer);

// Register Seller

const seller = require('./mongo/seller');
app.use('/api/seller', seller);

// Companies

const company = require('./mongo/company');
app.use('/api/company', company);

// Inbox

const inbox = require('./mongo/inbox');
app.use('/api/inbox', inbox);

// New Conversation
const newconversation = require('./mongo/newconversation');
app.use('/api/conversation', newconversation);

// Casestudies

const content = require('./mongo/content');
app.use('/api/content', content);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
// app.listen(port, '172.31.6.191', () => console.log(`Server started on port ${port}`));

