const express = require('express');
const router  = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const axios = require('axios')

/* POST login. */
router.post('/', function (req, res, next) {
    passport.authenticate('local', {session: false}, (err, user, info) => {
        if (err || !user) {
            return res.status(401).send({
                message: info.message
            });
        }
       req.login(user, {session: false}, (err) => {
           if (err) {
               res.send(err);
           }
           // generate a signed json web token with the contents of user object and return it in the response
           const token = jwt.sign(user, process.env.VUE_APP_JWT_SECRET);
           res.json({user, token});
        });
    })(req, res);
});

router.get('/auth', function (req, res, next) {
    const id = req.query.ID
        if(id) {
            axios.get(`http://localhost:5000/api/user/`, {
                    params: {
                      ID: id
                    }
                }).then((response) => {
                    const resdata = response.data[0]
                    delete resdata.password
                    res.send(resdata);
                })
                .catch((err)=> {
                    console.log(err);
                })
        } else {
        res.send('NOPE')
    }
});

module.exports = router;