const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
require('dotenv').config();
const mongoose = require('mongoose');
mongoose.connect(`mongodb://${process.env.VUE_APP_MONGODB_USER}:${process.env.VUE_APP_MONGODB_PW}@${process.env.VUE_APP_MONGODB_IP}/futurehealth`);

const Schema = mongoose.Schema;
const UserDetail = new Schema({
      username: String,
      password: String
    });
const UserModel = mongoose.model('users', UserDetail);


passport.use(new LocalStrategy(

    function (username, password, cb) {
        return UserModel.findOne({username})
           .then(user => {
               if (!user) {
                   return cb(null, false, {message: 'Incorrect username'});
               } 
                const hashpassword = user.password;
                bcrypt.compare(password, hashpassword, function(err, result) {
                    if(result) {
                        const userobj = user.toJSON()
                        delete userobj.password;
                        return cb(null, userobj, {message: 'Logged In Successfully'});
                    } else {
                        return cb(null, false, {message: 'Incorrect password'});
                    }
                });
          })
    }
));